package main

import (
	"fmt"
	"go/ast"
	"go/parser"
	"go/token"
	"log"
	"os"
)

type Risk struct {
	Id string
	Pos token.Pos
}

// var defaultInput = "./testdata/basic/main.go"
var defaultInput = "./testdata/constpropagation/main.go"

var unsafeCalls = map[string]bool{
	"SizeOf": true,
	"printIt": true,
}

func parseAst(filename string) (ast.File, *token.FileSet) {
	fset := token.NewFileSet()
	fileAst, err := parser.ParseFile(fset, filename, nil, parser.AllErrors)
	if err != nil {
		log.Fatal(err)
	}
	return *fileAst, fset
}

func traverseNode(node *ast.FuncDecl, fset *token.FileSet, decls []ast.Decl) ([]*Risk, error) {
	// fmt.Printf("%#v", node)
	// spew.Dump(node)

	risks := []*Risk{}

	for _, stmt := range node.Body.List {
		switch x := stmt.(type) {
		case *ast.ExprStmt:
			r, err := visitExprStmt(x)
			if err != nil {
				return risks, err
			}
			risks = append(risks, r)
		}
	}

	return risks, nil
}

func visitExprStmt(stmt *ast.ExprStmt) (*Risk, error) {
	switch x := stmt.X.(type) {
	case *ast.CallExpr:
		return visitCallExpr(x)
	default:
		return nil, nil
	}
}

func visitCallExpr(expr *ast.CallExpr) (*Risk, error) {
	var name string
	switch x := expr.Fun.(type) {
	case *ast.Ident:
		name = x.Name
	default:
		return nil, nil
	}

	if _, ok := unsafeCalls[name]; ok {
		return &Risk{Id: name, Pos: expr.Pos()}, nil
	}

	return nil, nil
}

func main() {
	input := defaultInput
	if len(os.Args) > 1 {
		input = os.Args[1]
	}
	fileAst, fset := parseAst(input)

	// fmt.Printf("%#v", fileAst)
	// spew.Dump(fileAst)
	var err error
	risks := []*Risk{}

	for _, n := range fileAst.Decls {
		switch x := n.(type) {
		case *ast.FuncDecl:
			risks, err = traverseNode(x, fset, fileAst.Decls)
		default:
			continue
		}
	}

	if err != nil {
		log.Fatal(err)
	}

	if len(risks) > 0 {
		fmt.Println("***")

		for i := range risks {
			r := risks[i]
			fmt.Printf("%v: %v\n", r.Id, fset.Position(r.Pos))
		}
	}
}