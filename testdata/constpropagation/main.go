package main

import (
	"fmt"
	"os"
)

var aconst = "console.log()"

func printIt(thing string) {
	fmt.Println(thing)
}

func main() {
	// Safe
	printIt(aconst)

	// Unsafe
	printIt(os.Args[0])
}
